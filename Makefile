# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/06/25 19:42:06 by lyhamrou          #+#    #+#              #
#    Updated: 2021/11/08 17:21:39 by lyhamrou         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME_LINK = libft_malloc.so
NAME = libft_malloc_$(HOSTTYPE).so
FLAGS = -Wall -Wextra -Werror
HEADER_PATH = includes/
HEADER_NAME = memory_manager.h ft_malloc.h
HEADER = $(addprefix $(HEADER_PATH), $(HEADER_NAME))
INCLUDE_HEADER = -I ./$(HEADER_PATH)
LIBFT_PATH = libft/
LIBFT_INC = -I $(LIBFT_PATH)
LIBFT_LINKER = -L $(LIBFT_PATH) -lft
LIBFT_ARG = $(LIBFT_INC) $(LIBFT_LINKER)
PTHREAD = -pthread

SRC_PATH = ./srcs/
SRC_NAME = ft_malloc.c ft_realloc.c ft_free.c tools.c block.c page.c show_alloc_mem.c ft_calloc.c
SRC = $(addprefix $(SRC_PATH), $(SRC_NAME))

OBJ_PATH = .obj/
OBJ_NAME = $(SRC_NAME:.c=.o)
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))

ifeq ($(HOSTTYPE),)
HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

OS_NAME = $(shell uname)
ifeq ($(OS_NAME), Darwin)
else
	OS_FLAGS += -fPIC
endif

all: $(NAME)

$(NAME): $(OBJ_PATH) $(OBJ)
	gcc -shared $(OS_FLAGS) $(FLAGS) $(LIBFT_ARG) -o $(NAME) $(INCLUDE_HEADER) $(OBJ)
	ln -s $(NAME) $(NAME_LINK)

$(OBJ_PATH):
	mkdir -p $(OBJ_PATH)
	make -C $(LIBFT_PATH)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(HEADER)
	gcc $(FLAGS) $(OS_FLAGS) $(INCLUDE_HEADER) $(LIBFT_INC) -o $@ -c $< $(PTHREAD)

clean:
	$(RM) -rf $(OBJ_PATH)
	make clean -C $(LIBFT_PATH)

fclean: clean
	$(RM) -rf $(NAME)
	$(RM) $(NAME_LINK)
	$(RM) mallockiller
	make fclean -C $(LIBFT_PATH)

re: fclean all

mallockiller: $(NAME) srcs/*.c includes/*.h
	gcc $(OS_FLAGS) -I $(HEADER_PATH) -o mallockiller srcs/main.c -L . -lft_malloc $(LIBFT_ARG)

.PHONY: all clean fclean re

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memory_manager.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/17 18:44:36 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/11/04 17:18:07 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <sys/resource.h> // int getrlimit (int resource, struct rlimit *rlim);
#include <sys/types.h>
#include <sys/stat.h>
#include "libft.h"

#define NB_ALLOC_MIN 128 // number of allocation minimum on a page
#define TINY_SIZE 256 // <<10=1024
#define SMALL_SIZE 1024 // 1<<12=4096


typedef unsigned char	byte;
typedef struct			s_page t_page;
typedef struct			s_block t_block;
typedef enum			e_blockType t_pageType;

/**
**	This enum is used to define a pageType, which each page contains a
**	particular kind of block
**/
typedef enum			e_blockType
{
	BLOCK_SIZE_ERROR = 0,
	TINY = 1,
	SMALL = 2,
	LARGE = 3
}						t_blockType;

struct					s_block
{
	size_t				size; // size of block in bytes
	bool				isFreed; __attribute__((aligned (16))) // aligned block memory
	void				*ptr; // address of the allocation (+ 24 bytes)
	t_page				*page; // address of the current page
	t_block				*next;
};

/**
**		This struccture contains a definition of a page, which contains a list
**		of blocks (only 1 block for a LARGE page)
**/
struct					s_page
{
	t_pageType			pageType; __attribute__((aligned (8)))// TINY, SMALL or LARGE
	size_t				remainingBlock; // nb of free block of the current page
	t_block				*block; // list of blocks of the page
	t_page				*next;
};

// global variables
extern t_page			*g_pages;
extern pthread_mutex_t	g_mutex;

// page functions
t_page					*pageFactory(t_blockType blockType, size_t size);
void					*createNewLargePage(size_t size);
t_page					*lastPageProvider(t_page *currentPage);
t_page					*freeSpacePageProvider(t_pageType pageType, t_page *pageList, size_t size);

// block functions
t_blockType				blockTypeProvider(size_t size);
void					*getBlockAddress(t_block *current);
void					*addBlock(t_page *currentPage, size_t size);
t_block					*lastBlockProvider(t_page *currentPage);

// tool functions
t_page					*ptrToPage(void *ptr);
t_blockType				sizeToBlockType(size_t size);
size_t					sizeToBlockSizeProvider(size_t size);
size_t					blockTypeToBlockSize(t_blockType blockType);
bool					isValidArea(void *ptr);
bool					isSevralPageType(t_page *currentPage);
bool					isOnlyOnePageType(t_pageType currentPageType);
size_t					findMaxAllocatedSize(void);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/17 18:44:28 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/07/08 17:36:12 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_H
#define FT_MALLOC_H

#include <sys/mman.h>
#include <pthread.h>
#include "memory_manager.h"

void	*malloc(size_t size);
void	free(void *ptr);
void	*realloc(void *ptr, size_t size);
void	*calloc(size_t nmemb, size_t size);

void	show_alloc_mem(void);
void	show_alloc_mem_more(void);
void	hexdump(char *str, void *data, int size);
void	hexDump(const void *data, size_t size);
void	printPage(int size);

#endif

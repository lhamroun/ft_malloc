/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pow.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/03 23:02:54 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/12/03 23:14:45 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long	calc_pow(long n, int p)
{
	int		i;
	long	nb;

	i = 0;
	nb = 1;
	if (p < 0)
		return (-1);
	else if (p == 0)
		return (1);
	else if (p == 1)
		return (n);
	while (i < p)
	{
		nb *= n;
		++i;
	}
	return (nb);
}

#include "ft_malloc.h"

void	*calloc(size_t nmemb, size_t size)
{
	void	*ptr;
	size_t	newSize;

//	write(1, "calloc\n", 7);
	ptr = NULL;
	newSize = nmemb * size;
	if (!nmemb || !size || newSize / nmemb != size)
		return (NULL);
	ptr = malloc(newSize);
	if (!ptr)
		return (NULL);
	ft_memset(ptr, 0, newSize);
	return (ptr);
}

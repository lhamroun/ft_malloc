/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/10 15:53:48 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/07/08 16:26:16 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

t_page            *g_pages = NULL;
pthread_mutex_t   g_mutex = PTHREAD_MUTEX_INITIALIZER;

void    *malloc(size_t size)
{
  t_blockType   blockType;
  t_page        *currentPage;
  void      *ptr;

//  write(2, "ft_malloc1\n", 11);
  ptr = NULL;
  currentPage = NULL;
  if (size == 0)
    return (NULL);
/*
  if (size > findMaxAllocatedSize())
  {
    write(1, "lala\n", 5);
    return (malloc(findMaxAllocatedSize()));
  }
*/
  blockType = blockTypeProvider(size);
  if (blockType == LARGE)
  {
    currentPage = (void *)pageFactory(blockType, size);
    currentPage->block->page = currentPage;
    ptr = currentPage->block->ptr;
  }
  else
  {
    if (!g_pages)
      if (!pageFactory(blockType, size))
        return (NULL);
    currentPage = freeSpacePageProvider(blockType, g_pages, size);
    ptr = addBlock(currentPage, size);
  }
  return (ptr);
}

/*
	printf("lala\n");
		if (g_pages)printf("g_pages next %p\n", g_pages->next);
		if (g_pages->next)printf("g_pages bloc %p\n", g_pages->next->block);
		printf("g_pages  %p\n", g_pages);
		printf("g_pages bloc %p\n", g_pages->block);
		if (g_pages->block)printf("g_pages bloc next %p\n", g_pages->block->next);
		printf("g_pages next %p\n", g_pages->next);
		
	printf("------------------\n");
	printf("ft_malloc1\n");
*/

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 16:05:52 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/07/05 16:48:51 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

extern t_page	*g_pages;

void	show_alloc_mem(void)
{
	size_t	total;
	t_page	*currentPage;
	t_block	*currentBlock;
	t_blockType	blockType;

	total = 0;
	currentPage = g_pages;
	if (currentPage)
	{
		while (currentPage)
		{
			currentBlock = currentPage->block;
			blockType = currentPage->pageType;
			write(1, blockType == TINY ? "TINY \n" : blockType == SMALL ? "SMALL\n" : "LARGE\n",  6);
			while (currentBlock)
			{
				total += currentBlock->size;
				write(1, "0x", 2);
				ft_putnbr_base_u((unsigned long)currentBlock->ptr, 16);
				write(1, " - 0x", 5);
				ft_putnbr_base_u((unsigned long)(currentBlock->ptr + currentBlock->size), 16);
				write(1, " : ", 3);
				ft_putnbr_base_u(currentBlock->size, 10);
				write(1, " octects\n", 9);
				currentBlock = currentBlock->next;
			}
			currentPage = currentPage->next;
		}
		write(1, "Total : ", 8);
		ft_putnbr_base_u(total, 10);
		write(1, " octects\n", 9);
	}
}

void    show_alloc_mem_more(void)
{
  int         i;
  int         j;
  t_page      *currentPage;
  t_block     *currentBlock;
  t_blockType blockType;

  i = 0;
  currentPage = g_pages;
  write(1, "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n", 35);
  write(1, "g_pages @ 0x", 12);
  ft_putnbr_base_u((long)currentPage, 16);
  write(1, "\n", 1);
  while (currentPage)
  {
    j = 0;
    currentBlock = currentPage->block;
    blockType = currentPage->pageType;
    write(1, "------------------------\n", 25);
    write(1, "-> page n ", 10);
    ft_putnbr(i + 1);
    write(1, " Type ", 6);
    write(1, blockType == TINY ? "TINY \n": blockType == SMALL ? "SMALL\n" : "LARGE\n", 6);
    write(1, "-> remaining Blocks   ", 23);
    ft_putnbr(currentPage->remainingBlock);
    write(1, "\n", 1);
    write(1, "-> from page          0x", 25);
    ft_putnbr_base_u((long)currentPage, 16);
    write(1, "\n", 1);
    while (currentBlock)
    {
      write(1, "           =-=-=-\n", 18);
      write(1, "        block n ", 16);
      ft_putnbr(j + 1);
      write(1, "\n", 1);
      write(1, "        block address 0x", 24);
      ft_putnbr_base_u((long)currentBlock, 16);
      write(1, "\n", 1);
      write(1, "        area address  0x", 24);
      ft_putnbr_base_u((long)currentBlock->ptr, 16);
      write(1, "\n", 1);
      write(1, "        block size ", 19);
      ft_putnbr_lu(currentBlock->size);
      write(1, "\n", 1);
      write(1, "        isFreed    ", 19);
      write(1, currentBlock->isFreed == 0 ? "false\n":"true \n", 6);
      write(1, "        from page n  ", 20);
      ft_putnbr(i);
      write(1, "\n", 1);
/*
      write(1, "        content of block :\n", 26);
      if (currentBlock->size < 65)
      {
        for (size_t i = 0; i < currentBlock->size; i++)
          printf(" %02hhx", *((unsigned char *)currentBlock->ptr + i));
        printf("\n");
      }
      else
        printf(" block is too large (%zu bytes) to print\n", currentBlock->size);
*/
      write(1, "           =-=-=-\n", 18);
      currentBlock = currentBlock->next;
      ++j;
    }
    currentPage = currentPage->next;
    ++i;
  }
    write(1, "------------------------\n", 25);
}

void hexDump(const void* data, size_t size)
{
	char ascii[17];
	size_t i, j;

	ascii[16] = '\0';
	printf("address of data %p\n", data);
	printf("size of data    %zu\n", size);
	for (i = 0; i < size; ++i) {
		if (i%16==0) printf("%04zx - ", i);
		printf("%02X ", ((unsigned char*)data)[i]);
		if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
			ascii[i % 16] = ((unsigned char*)data)[i];
		} else {
			ascii[i % 16] = '.';
		}
		if ((i+1) % 8 == 0 || i+1 == size) {
			printf(" ");
			if ((i+1) % 16 == 0) {
				printf("|  %s \n", ascii);
			} else if (i+1 == size) {
				ascii[(i+1) % 16] = '\0';
				if ((i+1) % 16 <= 8) {
					printf(" ");
				}
				for (j = (i+1) % 16; j < 16; ++j) {
					printf("   ");
				}
				printf("|  %s \n", ascii);
			}
		}
	}
}

void hexdump(char *desc, void* addr, int len)
{
	int i;
    unsigned char buff[17];
    unsigned char *pc = (unsigned char*)addr;

    if (desc != NULL)
        printf ("%s:\n", desc);
    for (i = 0; i < len; i++) {
        if ((i % 16) == 0) {
            if (i != 0)
                printf("  %s\n", buff);
            printf("  %04x ", i);
        }
        i % 16 == 7 ? printf(" %02x -", pc[i]) : printf(" %02x", pc[i]);
        if ((pc[i] < 0x20) || (pc[i] > 0x7e)) {
            buff[i % 16] = '.';
        } else {
            buff[i % 16] = pc[i];
        }
        buff[(i % 16) + 1] = '\0';
    }
    while ((i % 16) != 0) {
        printf("   ");
        i++;
    }
    printf("  %s\n", buff);
}


void	printPage(int size)
{
	if (!g_pages)
		printf("printPage: there is no page\n");
	else
	{
		for (t_page *currentPage = g_pages; currentPage; currentPage=currentPage->next)
		{
			hexdump("page ", currentPage, size);
			printf("_________________________\n");
		}
	}
}

/*
	printf("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
*/



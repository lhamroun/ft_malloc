/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   page.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/17 18:48:30 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/11/04 17:14:12 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_manager.h"

extern t_page	*g_pages;

t_page			*lastPageProvider(t_page *pageList)
{
	t_page	*current;

	if (!pageList)
		return (NULL);
	current = pageList;
	while (current->next)
		current = current->next;
	return (current);
}

void		*createNewLargePage(size_t size)
{
	t_page	*newPage;

//	printf("large size --> %zd\n", size + sizeof(t_page) + sizeof(t_block));
	newPage = (t_page *)mmap(0, size + sizeof(t_page) + sizeof(t_block), PROT_READ | PROT_WRITE,
						MAP_ANON | MAP_PRIVATE, -1, 0);
	if (newPage == MAP_FAILED)
		return (NULL);
	//ft_memset(newPage, 0, sizeof(t_page) + size + sizeof(t_block));
	newPage->pageType = LARGE;
	newPage->remainingBlock = 0;
	newPage->block = (t_block *)((void *)newPage + sizeof(t_page));
	newPage->block->ptr = (void *)((void *)newPage + sizeof(t_block) + sizeof(t_page));
	newPage->block->size = size;
	newPage->block->isFreed = 0;
	newPage->block->page = newPage;
	newPage->block->next = NULL;
	newPage->next = NULL;
	return (newPage);
}

void		pushNewPageToGlobalQueue(t_page *newPage)
{
	t_page		*current;

	current = g_pages;
	if (!current)
	{
		g_pages = newPage;
		return ;
	}
	while (current->next)
		current = current->next;
	current->next = newPage;
}

t_page		*createNewPage(t_blockType blockType)
{
	size_t	allocSize;
	t_page	*newPage;

	if (blockType != TINY && blockType != SMALL)
		return (NULL);
	allocSize = (sizeof(t_block) + blockTypeToBlockSize(blockType)) * NB_ALLOC_MIN + sizeof(t_page);
	allocSize = allocSize / getpagesize() + ((allocSize % getpagesize()) > 0);
	newPage = (t_page *)mmap(0, allocSize * getpagesize(),
			PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
	if (newPage == MAP_FAILED)
		return (NULL);
	//ft_memset(newPage, 0, allocSize * getpagesize());
	newPage->pageType = blockType;
  newPage->next = NULL;
	newPage->remainingBlock = (allocSize * getpagesize() - sizeof(t_page)) / (blockTypeToBlockSize(blockType) + sizeof(t_block));
//	printf("malloc page %p size %zu\n", newPage, sizeof(t_page) + allocSize * getpagesize());
	return (newPage);
}

t_page    *pageFactory(t_blockType blockType, size_t size)
{
  t_page  *newPage;

  if (blockType == LARGE)
    newPage = createNewLargePage(size);
  else
    newPage = createNewPage(blockType);
  if (!newPage)
    return (NULL);
  pushNewPageToGlobalQueue(newPage);
  return (lastPageProvider(g_pages));
}

t_page		*freeSpacePageProvider(t_pageType pageType, t_page *pageList, size_t size)
{
	t_page	*current;

	current = pageList;
	while (current)
	{
		if (current->pageType == pageType && current->remainingBlock > 0)
			return (current);
		current = current->next;
	}
	if (!current)
	{
		if (!pageFactory(pageType, size))
		{
			printf("freeSpacePageProvider4 erreur pagefacto\n");
			return (NULL);
		}
	}
	return (lastPageProvider(g_pages));
}

/*
	printf("---------------------\n");
	printf("Kangoo junior\n");
*/

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/29 17:47:11 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/11/08 16:12:21 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

extern t_page			*g_pages;

bool	checkIfCanMunmap(t_page *currentPage)
{
	unsigned int	i = 0;
	t_block	*currentBlock;

	currentBlock = currentPage->block;
	if (currentPage->pageType == LARGE)
		return (true);
	while (currentBlock)
	{
		if (!currentBlock->isFreed)
		{
			//			printf("NO we munmapn't\n");
			return (false);
		}
		currentBlock = currentBlock->next;
		++i;
	}
	//	printf("YES we munmap\n");
	return (true);
}

int		freePage(t_page *currentPage)
{
	int		result;
	size_t	size = 0;

	if (currentPage->pageType == LARGE)
		size = sizeof(t_page) + sizeof(t_block) + currentPage->block->size;
	else
	{
		size = sizeof(t_page) + NB_ALLOC_MIN * (blockTypeToBlockSize(currentPage->pageType) + sizeof(t_block));
		size = size / getpagesize() + (size % getpagesize() > 0);
		size = sizeof(t_page) + size * getpagesize();
	}
	result = munmap(currentPage, size);
	return (result);
}

void	pageJoiner(t_page *pageList, t_page *pageToDelete)
{
	t_page	*pageBefore;
	t_page	*pageNext;

	if (pageList && pageToDelete)
	{
		pageBefore = pageList;
		if (pageList == pageToDelete)
		{
			g_pages = pageList->next; // g_pages mais pas pageList ???????
			if (freePage(pageToDelete))
				printf("munmap fail\n");
			return ;
		}
		while (pageBefore->next)
		{
			if (pageBefore->next == pageToDelete)
				break ;
			pageBefore = pageBefore->next;
		}
		pageNext = pageToDelete->next;
		pageBefore->next = pageNext;
		if (freePage(pageToDelete))
			printf("munmap fail\n");
	}
	else
		printf("pageList %p - pageToDelete %p\n", pageList, pageToDelete);
}

void  cleanCurrentPage(t_page *pageToClean)
{
	t_block   *currentBlock;
	t_block   *tmp;
	size_t    allocSize;

	if (pageToClean->remainingBlock > 0)
		return ;
	currentBlock = pageToClean->block;
	while (currentBlock)
	{
		tmp = currentBlock->next;
		ft_memset(currentBlock, 0, sizeof(t_block));
		currentBlock = tmp;
	}
	allocSize = (sizeof(t_block) + blockTypeToBlockSize(pageToClean->pageType)) * NB_ALLOC_MIN + sizeof(t_page);
	allocSize = allocSize / getpagesize() + ((allocSize % getpagesize()) > 0);
	pageToClean->remainingBlock = (allocSize * getpagesize()) / (blockTypeToBlockSize(pageToClean->pageType) + sizeof(t_block));
}

void  free(void *ptr)
{
	t_block   *currentBlock;

	if (!ptr || !g_pages)
		return ;
	else
	{
		currentBlock = (t_block *)(ptr - sizeof(t_block));
		if (!isValidArea(ptr))
		{
			//		write(1, "not valid area\n", 15);
			return ;
		}
		currentBlock->isFreed = true;
		if (currentBlock->page->pageType != LARGE && isOnlyOnePageType(sizeToBlockType(currentBlock->size)))
			cleanCurrentPage(currentBlock->page);
		else if (checkIfCanMunmap(currentBlock->page))
		{
			pageJoiner(g_pages, currentBlock->page);
		}
	}
}

/*
//		printf("block     %p\n", currentBlock);
//		printf("is freed  %d\n", currentBlock->isFreed);
//		printf("size      %zd\n", currentBlock->size);
//		printf("input ptr %p\n", ptr);
//		printf("ptr       %p\n", currentBlock->ptr);
//		printf("page      %p\n", currentBlock->page);
//		printf("next      %p\n", currentBlock->next);
//		printf("pageJoiner4\n");
//	printf("----------------------\n");
//	printf("free1\n");
*/

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 13:37:04 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/07/08 18:54:45 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_manager.h"

extern t_page	*g_pages;

t_blockType		sizeToBlockType(size_t size)
{
	if (size == 0)
		return (BLOCK_SIZE_ERROR);
	else if (size <= TINY_SIZE)
		return (TINY);
	else if (size <= SMALL_SIZE)
		return (SMALL);
	else
		return (LARGE);
}

size_t			sizeToBlockSizeProvider(size_t size)
{
	if (size <= TINY_SIZE)
		return (TINY_SIZE);
	else if (size <= SMALL_SIZE)
		return (SMALL_SIZE);
	return (0);
}

size_t			blockTypeToSizeProvider(t_blockType blockType)
{
	if (blockType == TINY)
		return (TINY_SIZE);
	else if (blockType == SMALL)
		return (SMALL_SIZE);
	return (0);
}

t_page			*ptrToPage(void *ptr)
{
	t_block		*currentBlock;

	currentBlock = (t_block *)(ptr - sizeof(t_block));
	return (currentBlock->page);
}

bool			isValidArea(void *ptr)
{
	t_block		*currentBlock;
	size_t		tmp;

	if (!ptr)
	{
		return (false);
	}
	currentBlock = (t_block *)(ptr - sizeof(t_block));
	tmp = (size_t)currentBlock->isFreed;
	if (!currentBlock->page || !currentBlock->size || !currentBlock->ptr)
	{
		return (false);
	}
	if (tmp != 0 && tmp != 1)
	{
		return (false);
	}
	return (true);
}

bool	isSevralPageType(t_page *currentPage)
{
	t_page	*pageIterrator;

	pageIterrator = g_pages;

	while (pageIterrator)
	{
		if (currentPage->pageType == pageIterrator->pageType && currentPage != pageIterrator)
			return (false);
		pageIterrator = pageIterrator->next;
	}
	return (true);
}

bool	isOnlyOnePageType(t_pageType currentPageType)
{
	unsigned char	elem;
	t_page			*currentPage;

	elem = 0;
	currentPage = g_pages;
	while (currentPage)
	{
		if (currentPage->pageType == currentPageType)
			elem++;
		currentPage = currentPage->next;
	}
	if (elem == 1)
		return (true);
	else if (elem > 1)
		return (false);//printf("chaussure de balerine\n");
	return (false);
}

size_t  findMaxAllocatedSize(void)
{
  struct rlimit limit;

  getrlimit(RLIMIT_MEMLOCK, &limit);
  return (limit.rlim_max);
}

/*
	printf("chatoillant\n");
*/

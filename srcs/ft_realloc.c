/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/29 17:47:16 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/11/08 17:49:12 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

extern t_page     *g_pages;

void  *realloc(void *ptr, size_t size)
{
	t_blockType blockType;
	t_block     *currentBlock;
	void        *newPtr;

	newPtr = NULL;
	if (!size && !ptr)
		return (malloc(TINY_SIZE));
	if (!size)
	{
		free(ptr);
		return (NULL);
	}
	if (!isValidArea(ptr))
		return (NULL); // pour respecter le sujet
		//return (malloc(size));
	blockType = sizeToBlockType(size);
	currentBlock = (t_block *)(ptr - sizeof(t_block));
	if (blockType != LARGE && blockType == sizeToBlockType(currentBlock->size))
	{
		currentBlock->size = size;
		newPtr = ptr;
	}
	else
	{
		newPtr = malloc(size);
		if (newPtr)
			ft_strncpy(newPtr, ptr, ft_strlen((char *)ptr));
			//ft_memcpy(newPtr, ptr, currentBlock->size > size ? size : currentBlock->size);
		free(ptr);
		//ptr = NULL;
	}
	return (newPtr);
}

/*
//	printf("---------------------\n");
//	printf("realloc1\n");
//	printf("---------------------\n");
*/

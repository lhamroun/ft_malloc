/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   block.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/21 14:13:01 by lyhamrou          #+#    #+#             */
/*   Updated: 2021/07/05 17:40:39 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_manager.h"

extern t_page	*g_pages;

t_blockType		blockTypeProvider(size_t size)
{
	if (size <= TINY_SIZE)
		return (TINY);
	else if (size <= SMALL_SIZE)
		return (SMALL);
	else if (size > SMALL_SIZE)
		return (LARGE);
	else
		return (BLOCK_SIZE_ERROR);
}

size_t			blockTypeToBlockSize(t_blockType blockType)
{
	if (blockType == TINY)
		return (TINY_SIZE);
	else if (blockType == SMALL)
		return (SMALL_SIZE);
	return (0);
}

t_block		*lastBlockProvider(t_page *currentPage)
{
	t_block *currentBlock;

	if (!currentPage)// || !currentPage->block)
		return (NULL);
	currentBlock = currentPage->block;
	if (currentBlock)
	{
		while (currentBlock->next)
			currentBlock = currentBlock->next;
	}
	return (currentBlock);
}

size_t	getLastBlockAddress(t_page *currentPage)
{
	void	*tmp;
	size_t	address;

	tmp = (void *)lastBlockProvider(currentPage);
	if (!tmp)
		return ((size_t)currentPage + sizeof(t_page));
	address = (size_t)(tmp - (void *)currentPage);
	return (address);
}

void	fillBlock(t_block *newBlock, t_page *currentPage, size_t size)
{
	t_block	*lastBlock;

	lastBlock = lastBlockProvider(currentPage);
	newBlock->size = size;
	newBlock->page = currentPage;
	newBlock->isFreed = false;
	newBlock->ptr = (void *)newBlock + sizeof(t_block);
	if (lastBlock)
		lastBlock->next = newBlock;
	newBlock->next = NULL;
}

void	*addBlock(t_page *currentPage, size_t size)
{
	t_block		*newBlock;

	newBlock = NULL;
	if (currentPage->block && currentPage->remainingBlock > 0)
	{
		newBlock = (void *)lastBlockProvider(currentPage) + sizeof(t_block)
			+ blockTypeToBlockSize(currentPage->pageType);
	}
	else if (!currentPage->block)
	{
		newBlock = (t_block *)((void *)currentPage + sizeof(t_page));
		if (!newBlock)
			return (NULL);
		currentPage->block = newBlock;
	}
	fillBlock(newBlock, currentPage, size);
	currentPage->remainingBlock--;
	return (newBlock->ptr);
}

/*
//	printf("addBlock5\n");
//	printf("addBlock7\n");
//	printf("--------------------------\n");
	printf("Bonjourno\n");
*/
